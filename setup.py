import os
from setuptools import setup, find_packages
from pip._internal.req import parse_requirements


BASE_DIR = os.path.dirname(os.path.abspath(__file__))

__version__ = '0.2.0'

# parse_requirements() returns generator of pip.req.InstallRequirement objects
requirements = [str(ir.req) for ir in parse_requirements(os.path.join(BASE_DIR, 'requirements.txt'), session=False)]

setup(
    name='django-formtools-addons',
    author='The Cut',
    author_email='development@thecut.net.au',
    url='https://bitbucket.org/thecut/the-cut-formtools-addons/',
    version=__version__,
    packages=[
        'formtools_addons',
    ],
    include_package_data=True,
    install_requires=requirements,
    extras_require={},
    zip_safe=False,
    )
