# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import path
from django.views.decorators.csrf import ensure_csrf_cookie

from .views import TestWizard, TestWizardContainer


app_name = 'testapp'


test_wizard_container = TestWizardContainer.as_view()
test_wizard = TestWizard.as_view(url_name='testapp:accordeon_wizard_step')

urlpatterns = [
    path('<int:step>/', ensure_csrf_cookie(test_wizard), name='accordeon_wizard_step'),
    path('<int:step>/<int:substep>/', ensure_csrf_cookie(test_wizard), name='accordeon_wizard_step'),
    path('', test_wizard_container, name='accordeon_wizard'),
]
