# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.urls import path, include

urlpatterns = [
    # Frontend
    path('wizard/', include('testapp.urls', namespace='testapp'))
]
